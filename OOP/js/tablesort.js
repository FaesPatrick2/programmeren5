/*
In eventHandler wordt de sorteervolgorde bepaald aan de hand van de classname.
Deze classname wordt als parameter meegestuurd naar sortColumn().
Nadien wordt de classname van de TH en de innerHTML van de SPAN gewijzigd.

De sort()-methods van alpha[] en numeric[] houden rekening met de sortOrder-parameter (default is 'asc').
*/

function TableSort(id) {
    // When calling an object constructor or any of its methods, 
    // this’ refers to the instance of the object
    // much like any class-based language

    this.tableElement = document.getElementById(id);

    if (this.tableElement && this.tableElement.nodeName == "TABLE") {
        this.prepare();
    }
}

TableSort.prototype.prepare = function() {
    // add arrow up
    // default is ascending order

    var headings = this.tableElement.tHead.rows[0].cells;

    // headings is een htmlcollection

    for (let i = 0; i < headings.length; i++) {
        headings[i].innerHTML = headings[i].innerHTML + '<span>&nbsp;&nbsp;&uarr;</span>';
        headings[i].className = 'asc';
    }

    this.tableElement.addEventListener("click", function(that) {
        return function(event) {
            that.eventHandler(event);
        }
    }(this), false);
}

TableSort.prototype.eventHandler = function(event) {
    if (event.target.tagName === 'TH') {
        let sortingOrder = event.target.className;

        this.sortColumn(event.target, sortingOrder);

        if (sortingOrder == 'asc') {
            event.target.className = 'desc';
            event.target.querySelector('span').innerHTML = '&nbsp;&nbsp;&darr;'
        }
        else {
            event.target.className = 'asc';
            event.target.querySelector('span').innerHTML = '&nbsp;&nbsp;&uarr;'
        }
    }
    else if (event.target.tagName === 'SPAN') {
        if (event.target.parentNode.tagName === 'TH') {
            if (event.target.parentNode.className == "asc") {
                event.target.parentNode.className = 'desc';
                event.target.innerHTML = "&nbsp;&nbsp;&darr;"
            } else {
                event.target.parentNode.className = 'asc';
                event.target.innerHTML = "&nbsp;&nbsp;&uarr;"
            };
        }
    }
}

TableSort.prototype.sortColumn = function(headerCell, sortOrder = 'asc') {
    // Get cell data for column that is to be sorted from HTML table
    let rows = this.tableElement.rows;
    let alpha = [],
        numeric = [];
    let alphaIndex = 0,
        numericIndex = 0;
    let cellIndex = headerCell.cellIndex;

    for (var i = 1; rows[i]; i++) {
        let cell = rows[i].cells[cellIndex];
        let content = cell.textContent ? cell.textContent : cell.innerText;
        let numericValue = content.replace(/(\$|\,|\s)/g, "");
        
        if (parseFloat(numericValue) == numericValue) {
            numeric[numericIndex++] = {
                value: Number(numericValue),
                row: rows[i]
            }
        }
        else {
            alpha[alphaIndex++] = {
                value: content,
                row: rows[i]
            }
        }
    }

    let orderdedColumns = [];

    numeric.sort(function(a, b) {
        if (sortOrder == 'asc') {return a.value - b.value;}
        else {return b.value - a.value;}
    });

    alpha.sort(function(a, b) {
        let aName = a.value.toLowerCase();
        let bName = b.value.toLowerCase();

        if (aName < bName) {
            if (sortOrder == 'asc') {return -1;}
            else {return 1;}
        } else if (aName > bName){
            if (sortOrder == 'asc') {return 1;}
            else {return -1;}
        } else {
            return 0;
        }
    });

    // Reorder HTML table based on new order of data found in the col array
    orderdedColumns = numeric.concat(alpha);
    let tBody = this.tableElement.tBodies[0];

    for (let i = 0; orderdedColumns[i]; i++) {  
        tBody.appendChild(orderdedColumns[i].row);
    }
}   

window.onload = function() {
    var jommeke = new TableSort("jommeke");
    var fruit = new TableSort("fruit");
}