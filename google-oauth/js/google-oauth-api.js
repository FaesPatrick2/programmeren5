/**
 * In de functies signin en singout werd de code aangevuld met location.reload().
 * Er lijkt een probleem te zijn met het aanroepen van de listenere. Veel mensen melden dit op internetfora, maar een oplossing lijkt er niet te zijn.
 * Door de pagina te herladen, wordt de code opnieuw uitgevoerd.
 * In de functie signout wordt tevens de methode disconnect() aangeroepen. Dit logt de gebruiker weliswaar volledig af, maar anders kon de code niet worden uitgevoerd.
 */

var clientId = '625886195486-io4h8erqlse2uer6n32o5kmqavvot88s.apps.googleusercontent.com';
var apiKey = 'AIzaSyBc0UW13yqZTGhBPjajupuaL6ncDCHNQtI'; 
var scopes = 'profile';

var btnAanmelden = document.getElementById('btnAanmelden');
var btnAfmelden = document.getElementById('btnAfmelden');

function handleClientLoad() {
    gapi.load('client:auth2', initAuth);
}

function initAuth() {
    gapi.auth2.init({
        'apiKey': apiKey,
        client_id: clientId,
        scope: scopes
    }).then( function() {
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
		
        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
		
        btnAanmelden.addEventListener('click', signin);
        btnAfmelden.addEventListener('click', signout);   
    })
	.then(makePeopleApiCall)
    .then(showUserProfile)
    .catch( function(e) {
        alert(e.details);
    });
}

function updateSigninStatus(isSignedIn) {
	console.log('update: ' + isSignedIn);
	
    if (isSignedIn) {
        btnAanmelden.style.display = 'none';
        btnAfmelden.style.display = 'block';
    } else {
        btnAanmelden.style.display = 'block';
        btnAfmelden.style.display = 'none';
		
		let el = document.getElementById('content');
			while (el.firstChild) el.removeChild(el.firstChild);
		document.getElementById('userdata').innerHTML = '';
    }
}

function signin(event) {
    gapi.auth2.getAuthInstance().signIn().then( function() {location.reload();} ).catch( function(e) {console.log('signin: '+ e.details);} );
}

function signout(event) {
	if ( !window.confirm('Zeker dat je je wilt afmelden?') ) return;
	
	gapi.auth2.getAuthInstance().signOut().then( () => {gapi.auth2.getAuthInstance().disconnect();} ).then( () => {location.reload();} ).catch( function(e) {console.log('signin: '+ e.details);} );
}

function makePeopleApiCall() {
	let el = document.getElementById('content');
		while (el.firstChild) el.removeChild(el.firstChild);
	
    gapi.client.load('people', 'v1', function() {
        var request = gapi.client.people.people.get({
            resourceName: 'people/me',
            'requestMask.includeField': 'person.names'
        });

        request.execute( function(resp) {
            var p = document.createElement('p');

            if (resp.names) {
                var name = resp.names[0].givenName;
            }
            else {
                var name = 'Geen naam gevonden';
            }

            p.appendChild(document.createTextNode('Hello, ' + name + '!'));
            document.getElementById('content').appendChild(p);
                    
            var feedback = JSON.stringify(resp, null, 4);
            document.getElementById('content').appendChild(document.createTextNode(feedback));
        });
    });
}

function showUserProfile(resp) {
	document.getElementById('userdata').innerHTML = '';
	
	var profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
	
	var userdetails = [
		{field: 'Id', value: profile.getId()},
		{field: 'Name', value: profile.getName()},
		{field: 'Given name', value: profile.getGivenName()},
		{field: 'Family name', value: profile.getFamilyName()},
		{field: 'Image url', value: profile.getImageUrl()},
		{field: 'E-mail', value: profile.getEmail()},
	];
	
	for (var x in userdetails) {
		let field = userdetails[x].field;
		let value = userdetails[x].value;
		
		let tr = document.createElement('tr');
		let th = document.createElement('th');
		th.appendChild(document.createTextNode(field));
		tr.appendChild(th);
		let td = document.createElement('td');
		
		if (field == 'Image url') {
			let img = document.createElement('img');
			img.setAttribute('src', value);
			td.appendChild(img);
		}
		else {
			td.appendChild(document.createTextNode(value));
		}	
		
		tr.appendChild(td);
		
		document.getElementById('userdata').appendChild(tr);
	}
}

