const LikeButtonStyles = {
    button: {
            boxShadow: "inset 0px 1px 0px 0px #97c4fe",
            background: "linear-gradient(to bottom, #3d94f6 5%, #1e62d0 100%)",
            backgroundColor: "#3d94f6",
            borderRadius: "6px",
            border: "1px solid #337fed",
            display: "inline-block",
            cursor: "pointer",
            color: "#ffffff",
            fontFamily: "Arial",
            fontSize: "15px",
            fontWeight: "bold",
            padding: "6px 24px",
            marginRight: "5px",
            textShadow: "0px 1px 0px #1570cd"
        },

    label: {
            display: "inline-block",
            color: "#ffffff",
            backgroundColor: "#3d94f6",
            textShadow: "0px 1px 0px #1570cd",
            padding: "6px 24px",
            textShadow: "0px 1px 0px #1570cd",
            borderRadius: "6px",
            border: "1px solid #337fed"
        }
};

function AButton(props) {
    return <button onClick={props.clickHandler} style={LikeButtonStyles.button}>{props.caption}</button>
}

function ALabel(props) {
    return <label style={LikeButtonStyles.label}>{props.caption}</label>
}

class LikePanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            likes: 0
        }
    }

    addLike() {
        let newCount = this.state.likes + 1;

        this.setState({
            likes: newCount
        });
    }

    render() {
        //de bind-methode bepaalt waarnaar 'this' zal verwijzen binnen de context van de clickHandler
        return (<span><AButton clickHandler={this.addLike.bind(this)} caption="Like"/><ALabel caption={this.state.likes} /></span>);
    }
}

// ReactDOM.render(<LikePanel />, document.getElementById('one'));


//-------------------------



class Marvel extends React.Component {
    // we gaan veel keer createElement moeten gebruiken
    // we maken een afkorting
    e = React.createElement;
    marvelCharacterStyle = {
        list: {
            color: 'green',
            display: 'flex',
            flexWrap: 'wrap'
        },
        tile: {
            color: 'white',
            backgroundColor: 'red',
            textAlign: 'center',
            width: '10em',
            fontSize: '2em',
            fontFamily: 'Arial',
            paddingTop: '0.5em'
        },
        image: {
            color: 'blue',
            width: '10em',
            paddingTop: '0.5em'
        }
    };

    MarvelCharacter(props) {
        return (<div style={this.marvelCharacterStyle.tile} key={props.name}>
            {props.name}
            <img src={props.imageUrl}
                style={this.marvelCharacterStyle.image} alt={"foto van " + props.name} />
        </div>);
    }

    render() {
        return (
            <div>
                <h1>{this.props.heading}</h1>
                <div style={this.marvelCharacterStyle.list}>
                    {this.props.list.map(element => this.MarvelCharacter(element))}
                </div>
            </div>
        );
    }
}	