var l;
var r;
var w;
var timer;
var myInterval;
var gameDuration = 6000;
var timeLeft;
var timeInBetween = 10;

var startGame = function () {
    r.style.visibility = 'visible';
    timeLeft = gameDuration;
    myInterval = setInterval(showMessage, timeInBetween);
    timer = setTimeout(hideKnop, gameDuration);   
}

var endGame = function () {
    clearTimeout(timer);
    clearInterval(myInterval);
    w.innerText = 'Gefeliciteerd!';    
}

var hideKnop = function () {
    clearInterval(myInterval);
    r.style.visibility = 'hidden';
    w.innerText = '';
}

var showMessage = function () {
    timeLeft -= timeInBetween;
    
    if (timeLeft <= 0) {
        w.innerText = 'Tijd is op!';
        clearInterval(myInterval);
        return;
    }
    
    let msg = 'Raak snel de rode knop aan! Ze verdwijnt na 6 seconden.';

    //msg += ' Resterende tijd = ' + timeLeft; //(timeLeft/1000);

    w.innerText = msg;
}

function Init()
{
    l = document.getElementById('links');
    r = document.getElementById('rechts');
    w = document.getElementById('warning');

    l.onmouseover = startGame;
    r.onmouseover = endGame;
}

window.onload = Init;