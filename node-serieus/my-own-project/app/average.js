function calculateAverage(valuesList) {
   var n = valuesList.length;
   var total = 0;
   
   for (var x in valuesList) total += valuesList[x];

   return total/n;
}

module.exports.calculateAverage = calculateAverage;