const average = require('./average');

const valuesList = [25, 48, 97, 363, 182];
const calculatedAverage = average.calculateAverage(valuesList)

console.log(`The average of values ${valuesList.join(', ')} is ${calculatedAverage}.`);